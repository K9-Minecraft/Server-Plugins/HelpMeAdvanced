package com.gmail.kyle.corsi.helpmeadvanced;

import com.gmail.kyle.corsi.helpmeadvanced.database.DBHelper;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Ticket;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Ticket.TicketStatus;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class HelpMeCommand {

    DBHelper _dbHelper;

    public HelpMeCommand(DBHelper dbHelper) {
        _dbHelper = dbHelper;
	}

	public CommandExecutor BaseCommand = new CommandExecutor(){
            @Override
            public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            //Check for message arguments
                String message = args.<String>getOne("message").get();
                Player player = (Player) src;
                Ticket newTicket;
				try {
                    newTicket = _dbHelper.createTicket(player.getName(), player.getUniqueId().toString(), message, player.getLocation());
                    Text returnMessage = Text.builder("Your ticket has been submitted with ID:").color(TextColors.GREEN).append(
                    Text.builder(Integer.toString(newTicket.getId())).color(TextColors.YELLOW).build()).build();
                    src.sendMessage(returnMessage);
                    AlertAdmins(newTicket);
                    return CommandResult.success();
				} catch (Exception e) {
                    Text returnMessage = Text.builder("Could not submit ticket due to error").color(TextColors.RED).build();
                    throw new CommandException(returnMessage);
				}
                           
        }
    };

    private void AlertAdmins(Ticket ticket) {
        Collection<Player> onlinePlayers = Sponge.getServer().getOnlinePlayers();
        for(Player player:onlinePlayers) {
            if (player.hasPermission("helpme.admin")) {
                player.sendMessage(Text.builder("New ticket submitted with ID:" + ticket.getId()).color(TextColors.YELLOW).build());
            }
        }
    }

    public CommandExecutor ListCommand = new CommandExecutor(){
    
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            List<Text> returnMessage = new ArrayList<Text>();
            returnMessage.add(
                Text.builder("===Unresolved Tickets===").color(TextColors.DARK_RED).build()
            );
            List<Ticket> allTickets = _dbHelper.getTickets();
            Collections.sort(allTickets, new SortbyId());
            for(Ticket ticket:allTickets) {
                if (ticket.getStatus() != TicketStatus.Closed) {
                    String[] messages = _dbHelper.getMessages(ticket.getId());
                    String finalMessage;
                    if (messages[0].length() > 15) {
                        finalMessage = messages[0].substring(0, 14);
                    } else {
                        finalMessage = messages[0];
                    }
                    returnMessage.add(
                        Text.builder(ticket.getId() + ":" + ticket.getStatus() + ":" + (messages.length > 0 ? finalMessage : "No message...")).build()
                    );
                }
            }
            returnMessage.add(
                Text.builder("===End of Tickets===").color(TextColors.DARK_RED).build()
            );
            src.sendMessages(returnMessage);
            return CommandResult.success();
        }
    };

    public CommandExecutor ViewCommand = new CommandExecutor(){
    
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String ticketNumber = args.<String>getOne("ticketnum").get();
            Ticket resultTicket = _dbHelper.getTicket(ticketNumber);
            List<Text> resultMessage = new ArrayList<Text>();
            resultMessage.add(Text.builder("===Ticket:" + resultTicket.getId() + ":").color(TextColors.YELLOW).append(
                Text.builder(resultTicket.getStatus().toString()).color(TextColors.BLUE).append(
                    Text.builder("===").color(TextColors.YELLOW).build()
                ).build()).build());
            int i = 0;
            String[] messages = _dbHelper.getMessages(resultTicket.getId());
            for (String message: messages) {
                i++;
                resultMessage.add(Text.builder(i + ". ").color(TextColors.GREEN).append(Text.builder(message).build()).build());
            }
            if (resultTicket.getId() != 0) {
                resultMessage.add(Text.builder("===End of Ticket===").color(TextColors.YELLOW).build());
                src.sendMessages(resultMessage);
                return CommandResult.success();
            } else {
                throw new CommandException(Text.of("Couldn't find a ticket with that number"));
            }
        }
    };

    public CommandExecutor ChangeStatusCommand = new CommandExecutor(){
    
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String ticketNumber = args.<String>getOne("ticketnum").get();
            TicketStatus ticketStatus = args.<TicketStatus>getOne("status").get();
            boolean commandSuccess = _dbHelper.changeStatus(ticketNumber, ticketStatus);
            if (commandSuccess) {
                Text returnMessage = Text.builder("===Ticket:" + ticketNumber + " Updated!===").color(TextColors.YELLOW).build();
                src.sendMessage(returnMessage);
                return CommandResult.success();
            } else {
                Text returnMessage = Text.builder("===Ticket:" + ticketNumber + " Unable to be updated===").color(TextColors.YELLOW).build();
                throw new CommandException(returnMessage);
            }
        }
    };

    public CommandExecutor GotoTicketCommand = new CommandExecutor(){
    
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String ticketNumber = args.<String>getOne("ticketnum").get();
            Ticket ticket = _dbHelper.getTicket(ticketNumber);
            Player player = (Player)src;
            World extent = Sponge.getServer().getWorld(ticket.getWorldName()).get();
            player.setLocation(new Location<World>(extent, ticket.getX(), ticket.getY(), ticket.getZ()));
            return CommandResult.success();
        }
    };

    public CommandExecutor AddMessageCommand = new CommandExecutor(){
    
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            Integer ticketNumber = args.<Integer>getOne("ticketnum").get();
            String message = args.<String>getOne("message").get();
            boolean commandSuccess = _dbHelper.createMessage(ticketNumber, message);
            if (commandSuccess) {
                Text returnMessage = Text.builder("===Ticket:" + ticketNumber + " Updated!===").color(TextColors.YELLOW).build();
                src.sendMessage(returnMessage);
                return CommandResult.success();
            } else {
                Text returnMessage = Text.builder("Unable to update ticket with your message").color(TextColors.RED).build();
                throw new CommandException(returnMessage);
            }
        }
    };

    public CommandExecutor CloseTicketCommand = new CommandExecutor(){
    
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String ticketNumber = args.<String>getOne("ticketnum").get();
            boolean commandSuccess = _dbHelper.changeStatus(ticketNumber, TicketStatus.Closed);
            if (commandSuccess) {
                Text returnMessage = Text.builder("===Ticket:" + ticketNumber + " Closed!===").color(TextColors.YELLOW).build();
                src.sendMessage(returnMessage);
                return CommandResult.success();
            } else {
                Text returnMessage = Text.builder("===Ticket:" + ticketNumber + " Unable to be closed===").color(TextColors.YELLOW).build();
                throw new CommandException(returnMessage);
            }
        }
    };
}

class SortbyId implements Comparator<Ticket>
{
    // Used for sorting in ascending order of ID number
    public int compare(Ticket a, Ticket b)
    {
        return a.getId() - b.getId();
    }
}