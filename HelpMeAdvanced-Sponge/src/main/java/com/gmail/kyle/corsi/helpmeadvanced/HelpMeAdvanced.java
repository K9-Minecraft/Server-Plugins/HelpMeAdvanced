package com.gmail.kyle.corsi.helpmeadvanced;

import com.gmail.kyle.corsi.helpmeadvanced.database.DBHelper;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Ticket.TicketStatus;
import com.google.inject.Inject;

import org.slf4j.Logger;
import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

@Plugin(id="helpmeadvanced", name="HelpMe Advanced", version="1.0.0", description="A plugin to aid in the creation and management of support tickets")
public final class HelpMeAdvanced {
    
    @Inject
    private Game _game;
    @Inject
    private Logger _logger;

    private DBHelper _dbHelper;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        //Schedule registerCommands function to run
        this._game.getScheduler().createAsyncExecutor(this).execute(this::registerCommands);
    }

    @Listener
    public void onGameInitialized(GameInitializationEvent event) {
        //Schedule checkForUpdates function to run
        this._game.getScheduler().createAsyncExecutor(this).execute(this::checkForUpdates);
        //Schedule loading data from DB
        this._dbHelper = new DBHelper("jdbc:h2:./database/HelpMe Advanaced/hma", _logger);
    }

    @Listener
    public void onServerStop(GameStoppedServerEvent event) {
        //Close DB connection softly
        _dbHelper.CloseDB();
    }

    private void registerCommands() {
        HelpMeCommand helpMeCommand = new HelpMeCommand(_dbHelper);
        CommandSpec helpMeAdminListCommand = CommandSpec.builder()
        .description(Text.of("HelpMe Advanced Admin. List non-closed tickets"))
        .permission("helpme.command.list")
        .executor(helpMeCommand.ListCommand)
        .build();

        CommandSpec helpMeAdminViewCommand = CommandSpec.builder()
        .description(Text.of("HelpMe Advanced Admin. View messages and status for a ticket"))
        .permission("helpme.command.view")
        .arguments(GenericArguments.string(Text.of("ticketnum")))
        .executor(helpMeCommand.ViewCommand)
        .build();

        CommandSpec helpMeAdminGotoCommand = CommandSpec.builder()
        .description(Text.of("HelpMe Advanced Admin. Goto coordinates where ticket was originally created"))
        .permission("helpme.command.goto")
        .arguments(GenericArguments.string(Text.of("ticketnum")))
        .executor(helpMeCommand.GotoTicketCommand)
        .build();

        CommandSpec helpMeAdmingChangeStatusCommand = CommandSpec.builder()
        .description(Text.of("HelpMe Advanced Admin. Updated the status of the ticket to 'Open', 'Needs More Info'"))
        .permission("helpme.command.status")
        .arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("ticketnum"))),
        GenericArguments.onlyOne(GenericArguments.enumValue(Text.of("status"), TicketStatus.class)))
        .executor(helpMeCommand.ChangeStatusCommand)
        .build();

        CommandSpec helpMeCommandAddMessage = CommandSpec.builder()
        .description(Text.of("HelpMe Advanced. Add another message to the ticket"))
        .permission("helpme.command.message")
        .arguments(GenericArguments.onlyOne(GenericArguments.integer(Text.of("ticketnum"))),
        GenericArguments.remainingJoinedStrings(Text.of("message")))
        .executor(helpMeCommand.AddMessageCommand)
        .build();

        CommandSpec helpMeAdminCloseCommand = CommandSpec.builder()
        .description(Text.of("HelpMe Advanced Admin. Close ticket, alerting submitter and removing from Admin queue"))
        .permission("helpme.command.close")
        .arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("ticketnum"))))
        .executor(helpMeCommand.CloseTicketCommand)
        .build();

        CommandSpec helpMeBaseCommand = CommandSpec.builder()
        .description(Text.of("HelpMe Advanced Command"))
        .permission("helpme.command.use")
        .child(helpMeAdminListCommand, "list")
        .child(helpMeAdminViewCommand, "view")
        .child(helpMeAdminGotoCommand, "goto")
        .child(helpMeAdmingChangeStatusCommand, "status")
        .child(helpMeCommandAddMessage, "message")
        .child(helpMeAdminCloseCommand, "close")
        .arguments(GenericArguments.remainingJoinedStrings(Text.of("message")))
        .executor(helpMeCommand.BaseCommand)
        .build();


        Sponge.getCommandManager().register(this, helpMeBaseCommand, "helpme");
    }

    private void checkForUpdates() {
        //TBD
    }
}