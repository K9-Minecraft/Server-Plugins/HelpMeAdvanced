package com.gmail.kyle.corsi.helpmeadvanced.database;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.flowpowered.math.vector.Vector3d;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Message;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Ticket;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Ticket.TicketStatus;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import org.slf4j.Logger;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class DBHelper {
    
    private Logger _logger;
    public ArrayList<Ticket> _tickets;
    Dao<Ticket, String> _ticketDao;
    Dao<Message, String> _messageDao;
    private ConnectionSource _connSource;

    public DBHelper(String jdbcUrl, Logger logger) {
        _logger = logger;
        //Get Sponge SQL Service
        try {
            //Define connection source, DAO and create the table if it doesn't exist
            ConnectionSource connectionSource = new JdbcConnectionSource(jdbcUrl);
            _ticketDao = DaoManager.createDao(connectionSource, Ticket.class);
            _messageDao = DaoManager.createDao(connectionSource, Message.class);
            TableUtils.createTableIfNotExists(connectionSource, Ticket.class);
            TableUtils.createTableIfNotExists(connectionSource, Message.class);
            _ticketDao.executeRawNoArgs("ALTER TABLE `tickets` ALTER `uuid` SET DATA TYPE varchar(255)");
            _connSource = connectionSource;
        } catch (SQLException ex) {
            _logger.error(String.format("Error connecting to DB:%s", ex.getMessage()));
        } catch (Exception ex) {
            _logger.error(String.format("Error initializing DBHelper:%s%s%s", ex.getMessage(), System.lineSeparator(), stackTraceToString(ex)));
        }
    }

    public Ticket createTicket(String user, String uuid, String message, Location<World> location) throws Exception {
        Ticket ticket = new Ticket();
        ticket.setStatus(TicketStatus.Open);
        ticket.setUuid(uuid);
        Vector3d position = location.getPosition();
        String worldName = location.getExtent().getName();
        ticket.setX(position.getX());
        ticket.setY(position.getY());
        ticket.setZ(position.getZ());
        ticket.setWorldName(worldName);
        try
        {
            _ticketDao.create(ticket);
            createMessage(ticket.getId(), message);
            return ticket;
            //Set messages for this ticket
        } catch (SQLException sqlEx) {
            _logger.error(String.format("Error saving ticket because:%s%s%s", sqlEx.getMessage(),System.lineSeparator(),stackTraceToString(sqlEx)));
            throw new Exception("Error saving ticket");
        } catch (Exception ex) {
            _logger.error(String.format("Unknown error occured saving ticket because:%s%s%s", ex.getMessage(),System.lineSeparator(),stackTraceToString(ex)));
            throw new Exception("Error saving ticket");
        }
    }

    public Boolean createMessage(int ticketId, String message) {
        Message newMessage = new Message();
        try
        {
            newMessage.setMessage(message);
            newMessage.setTicketId(ticketId);
            _messageDao.create(newMessage);
            return true;
        } catch (SQLException sqlEx) {
            _logger.error(String.format("Error saving ticket because:%s%s%s", sqlEx.getMessage(),System.lineSeparator(),stackTraceToString(sqlEx)));
            return false;
        } catch (Exception ex) {
            _logger.error(String.format("Unknown error occured saving ticket because:%s%s%s", ex.getMessage(),System.lineSeparator(),stackTraceToString(ex)));
            return false;
        }
	}

    public List<Ticket> getTickets() {
        List<Ticket> resultTickets = new ArrayList<>();
        try {
            resultTickets = _ticketDao.queryForEq("status", TicketStatus.Open);
            resultTickets.addAll(_ticketDao.queryForEq("status", TicketStatus.Pending));
            resultTickets.addAll(_ticketDao.queryForEq("status", TicketStatus.Other));
            return resultTickets;
        } catch (SQLException sqlEx) {
            _logger.error(String.format("Error initializing ticket data from the database because:%s%s%s", sqlEx.getMessage(),System.lineSeparator(),stackTraceToString(sqlEx)));
            return resultTickets;
        } catch (Exception ex) {
            _logger.error(String.format("Unknown error occurred initializing ticket data from the database because:%s%s%s", ex.getMessage(),System.lineSeparator(),stackTraceToString(ex)));
            return resultTickets;
        }
    }

    public Ticket getTicket(String id) {
        Ticket resultTicket = new Ticket();
        try {
            resultTicket = _ticketDao.queryForId(id);
            return resultTicket;
        } catch (SQLException sqlEx) {
            _logger.error(String.format("Error fetching ticket data from the database because:%s%s%s", sqlEx.getMessage(),System.lineSeparator(),stackTraceToString(sqlEx)));
            return resultTicket;
        }
    }

	public String[] getMessages(int id) {
        try {
            List<String> messages = _messageDao.queryForEq("ticketId", id).stream().map(Message::getMessage).collect(Collectors.toList());
            String[] result = new String[messages.size()];
            result = messages.toArray(result);
            return result;
        } catch (SQLException sqlEx) {
            _logger.error(String.format("Error initializing ticket data from the database because:%s%s%s", sqlEx.getMessage(),System.lineSeparator(),stackTraceToString(sqlEx)));
            return new String[0];
        } catch (Exception ex) {
            _logger.error(String.format("Unknown error occurred initializing ticket data from the database because:%s%s%s", ex.getMessage(),System.lineSeparator(),stackTraceToString(ex)));
            return new String[0];
        }
	}

	public void CloseDB() {
        try {
            _connSource.close();
        } catch (IOException ex) {
            _logger.error(String.format("Error closing connection to DB:%s", ex.getMessage()));
        }
    }

    public boolean changeStatus(String id, TicketStatus status) {
        try {
            Ticket ticket = _ticketDao.queryForId(id);
            ticket.setStatus(status);
            _ticketDao.update(ticket);
            return true;
        } catch (SQLException sqlEx) {
            _logger.error(String.format("Error changing status of ticket:%s because:%s", id, sqlEx.getMessage()));
            return false;
        }
    }

    private String stackTraceToString(Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String sStackTrace = sw.toString(); // stack trace as a string
        return sStackTrace;
    }
}