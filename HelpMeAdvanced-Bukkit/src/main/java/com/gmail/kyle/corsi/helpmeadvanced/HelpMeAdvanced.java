package com.gmail.kyle.corsi.helpmeadvanced;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import com.gmail.kyle.corsi.helpmeadvanced.database.DBHelper;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Ticket;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Ticket.TicketStatus;
import com.google.common.collect.ImmutableList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class HelpMeAdvanced extends JavaPlugin {
    DBHelper _dbHelper;

    @Override
    public void onEnable() {
        _dbHelper = new DBHelper("jdbc:h2:./database/HelpMe Advanaced/hma", getLogger());
    }

    @Override
    public void onDisable() {
        _dbHelper.CloseDB();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        // do something
        if (command.getName().equalsIgnoreCase("helpme")) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                return parseArgs(args, player);
            } else {
                sender.sendMessage("You must be a player!");
                return false;
            }
        } else {
            return false;
        }
    }

	private boolean parseArgs(String[] args, Player sender) {
        if (args.length == 0) {
            sender.sendMessage(ChatColor.RED + "Please include a message with your ticket");
            return false;
        } else if (args.length > 0) {
            Bukkit.getLogger().info("Args length:" + args.length);
            for (String arg : args) {
                Bukkit.getLogger().info("Arg:" + arg);
            }
            if (args[0].equalsIgnoreCase("close")) {
                if (args.length != 2) {
                    sender.sendMessage(ChatColor.RED + "To close a ticket you must specify only the ticket number");
                    return false;
                } else {
                    return closeTicket(args[1], sender);
                }
            } else if (args[0].equalsIgnoreCase("status")) {
                if (args.length != 3) {
                    sender.sendMessage(ChatColor.RED + "To change a ticket status you must specify the ticket number and one of the permitted statuses");
                    return false;
                } else {
                    return changeStatus(args[1], TicketStatus.valueOf(args[2]), sender);
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                return listTickets(sender);
            } else if (args[0].equalsIgnoreCase("goto")) {
                if (args.length != 2) {
                    sender.sendMessage(ChatColor.RED + "To goto a ticket you must specify only the ticket number");
                    return false;
                } else {
                    return gotoTicket(args[1], sender);
                }
            } else if (args[0].equalsIgnoreCase("message")) {
                if (args.length <= 2) {
                    sender.sendMessage(ChatColor.RED + "To add a message to a ticket you must specify the ticket number and a message");
                    return false;
                } else {
                    return addMessage(args, sender);
                }
            } else {
                //Is a ticket submission
                return createTicket(args, sender);
            }
        }
		return false;
	}

	private void AlertAdmins(Ticket createdTicket) {
        ImmutableList<Player> onlinePlayers = ImmutableList.copyOf(Bukkit.getOnlinePlayers());
        for(Player player:onlinePlayers) {
            if (player.hasPermission("helpme.admin")) {
                player.sendMessage(ChatColor.YELLOW + "New ticket submitted with ID:" + createdTicket.getId());
            }
        }
        
	}

	private boolean createTicket(String[] args, Player sender) {
		String fullMessage = "";
        for (int i = 1; i < args.length; i++) {
            fullMessage += args[i];
        }
        try {
            Ticket createdTicket = _dbHelper.createTicket(sender.getName(), sender.getUniqueId().toString(), fullMessage, sender.getLocation());
            sender.sendMessage(ChatColor.GREEN + "Your ticket has been submitted with ID:" + ChatColor.YELLOW + createdTicket.getId());
            AlertAdmins(createdTicket);
            return true;
        } catch (Exception e) {
            sender.sendMessage(ChatColor.RED + "Could not submit ticket due to error");
            return false;
        }
	}

	private boolean gotoTicket(String ticketNumber, Player sender) {
        Ticket ticket = _dbHelper.getTicket(ticketNumber);
        sender.teleport(new Location(Bukkit.getWorld(ticket.getWorldName()), ticket.getX(), ticket.getY(), ticket.getZ()));
        return true;
	}

	private boolean listTickets(Player sender) {
        List<String> returnMessage = new ArrayList<String>();
        returnMessage.add(ChatColor.DARK_RED + "===Unresolved Tickets===");
        List<Ticket> allTickets = _dbHelper.getTickets();
        Collections.sort(allTickets, new SortbyId());
        for(Ticket ticket:allTickets) {
            if (ticket.getStatus() != TicketStatus.Closed) {
                String[] messages = _dbHelper.getMessages(ticket.getId());
                String finalMessage;
                if (messages[0].length() > 15) {
                    finalMessage = messages[0].substring(0, 14);
                } else {
                    finalMessage = messages[0];
                }
                returnMessage.add(ticket.getId() + ":" + ticket.getStatus() + ":" + (messages.length > 0 ? finalMessage : "No message..."));
            }
        }
        returnMessage.add(ChatColor.DARK_RED + "===End of Tickets===");
        sender.sendMessage(returnMessage.toArray(new String[returnMessage.size()]));
        return true;
	}

	private boolean changeStatus(String ticketNumber, TicketStatus status, Player sender) {
        boolean commandSuccess = _dbHelper.changeStatus(ticketNumber, status);
        if (commandSuccess) {
            sender.sendMessage(ChatColor.YELLOW + "===Ticket:" + ticketNumber + " Updated!===");
            return true;
        } else {
            sender.sendMessage(ChatColor.YELLOW + "===Ticket:" + ticketNumber + " Unable to be updated===");
            return false;
        }
	}

    private boolean closeTicket(String ticketNumber, Player sender) {
        boolean commandSuccess = _dbHelper.changeStatus(ticketNumber, TicketStatus.Closed);
        if (commandSuccess) {
            sender.sendMessage(ChatColor.YELLOW + "===Ticket:" + ticketNumber + " Closed!===");
            return true;
        } else {
            sender.sendMessage(ChatColor.YELLOW + "===Ticket:" + ticketNumber + " Unable to be closed===");
            return false;
        }
    }
    
    private boolean addMessage(String[] args, Player sender) {
        /*
        Integer ticketNumber = args.<Integer>getOne("ticketnum").get();
            String message = args.<String>getOne("message").get();
            boolean commandSuccess = _dbHelper.createMessage(ticketNumber, message);
            if (commandSuccess) {
                Text returnMessage = Text.builder("===Ticket:" + ticketNumber + " Updated!===").color(TextColors.YELLOW).build();
                src.sendMessage(returnMessage);
                return CommandResult.success();
            } else {
                Text returnMessage = Text.builder("Unable to update ticket with your message").color(TextColors.RED).build();
                throw new CommandException(returnMessage);
            }
        */
        String message = "";
        for(int i = 2; i < args.length; i++) {
            message += args[i] + " ";
        }
        boolean commandSuccess = _dbHelper.createMessage(Integer.parseInt(args[1]), message);
        if (commandSuccess) {
            String returnMessage = ChatColor.YELLOW + "===Ticket:" + args[1] + " Updated!===";
            sender.sendMessage(returnMessage);
            return true;
        } else {
            String returnMessage = ChatColor.RED + "Unable to update ticket with your message";
            sender.sendMessage(returnMessage);
            return false;
        }
    }
}

class SortbyId implements Comparator<Ticket>
{
    // Used for sorting in ascending order of ID number
    public int compare(Ticket a, Ticket b)
    {
        return a.getId() - b.getId();
    }
}