package com.gmail.kyle.corsi.helpmeadvanced.database.schemas;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "messages")
public class Message
{
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private int ticketId;

    @DatabaseField
    private String message;

    public Message() {
        
    }

    public int getId ()
    {
        return id;
    }

    public void setId (int id)
    {
        this.id = id;
    }

    public int getTicketId()
    {
        return ticketId;
    }

    public String getMessage()
    {
        return message;
    }

    public void setTicketId (int ticketId)
    {
        this.ticketId = ticketId;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }
}