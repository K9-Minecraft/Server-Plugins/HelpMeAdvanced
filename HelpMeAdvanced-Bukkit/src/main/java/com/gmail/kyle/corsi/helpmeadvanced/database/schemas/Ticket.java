package com.gmail.kyle.corsi.helpmeadvanced.database.schemas;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tickets")
public class Ticket
{
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private TicketStatus status;

    @DatabaseField
    private String uuid;

    @DatabaseField
    private double x;

    @DatabaseField
    private double y;

    @DatabaseField
    private double z;

    @DatabaseField
    private String worldName;

    public Ticket() {

    }

    public enum TicketStatus {
        Open,
        Closed,
        Pending,
        Other
    }

    public int getId ()
    {
        return id;
    }

    public void setId (int id)
    {
        this.id = id;
    }

    public TicketStatus getStatus ()
    {
        return status;
    }

    public void setStatus (TicketStatus status)
    {
        this.status = status;
    }

    public String getUuid ()
    {
        return uuid;
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public double getZ()
    {
        return z;
    }

    public String getWorldName()
    {
        return worldName;
    }

    public void setUuid (String uuid)
    {
        this.uuid = uuid;
    }

    public void setX(double x) 
    {
        this.x = x;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    public void setZ(double z)
    {
        this.z = z;
    }

    public void setWorldName(String name)
    {
        this.worldName = name;
    }
}