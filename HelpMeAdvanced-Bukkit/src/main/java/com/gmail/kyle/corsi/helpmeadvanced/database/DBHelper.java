package com.gmail.kyle.corsi.helpmeadvanced.database;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.PortUnreachableException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Message;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Ticket;
import com.gmail.kyle.corsi.helpmeadvanced.database.schemas.Ticket.TicketStatus;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class DBHelper {
    private Dao<Ticket, String> _ticketDao;
    private Dao<Message, String> _messageDao;
    private ConnectionSource _connSource;
    private Logger _logger;
    
    public DBHelper(String jdbcUrl, Logger logger) {
        _logger = logger;
        try {
            ConnectionSource connectionSource = new JdbcConnectionSource(jdbcUrl);
            _ticketDao = DaoManager.createDao(connectionSource, Ticket.class);
            _messageDao = DaoManager.createDao(connectionSource, Message.class);
            TableUtils.createTableIfNotExists(connectionSource, Ticket.class);
            TableUtils.createTableIfNotExists(connectionSource, Message.class);
            _ticketDao.executeRawNoArgs("ALTER TABLE `tickets` ALTER `uuid` SET DATA TYPE varchar(255)");
            _connSource = connectionSource;
        } catch (SQLException ex) {
            _logger.severe(String.format("Error connecting to DB:%s", ex.getMessage()));
        }
    }

    public Ticket createTicket(String user, String uuid, String message, Location location) throws Exception {
        Ticket ticket = new Ticket();
        ticket.setStatus(TicketStatus.Open);
        ticket.setUuid(uuid);
        ticket.setX(location.getX());
        ticket.setY(location.getY());
        ticket.setZ(location.getZ());
        ticket.setWorldName(location.getWorld().getName());
        try
        {
            _ticketDao.create(ticket);
            createMessage(ticket.getId(), message);
            return ticket;
        } catch (SQLException sqlEx) {
            _logger.severe(String.format("Error saving ticket because:%s%s%s", sqlEx.getMessage(), System.lineSeparator(), stackTraceToString(sqlEx)));
            throw new Exception("Error saving ticket");
        }
    }

	public Boolean createMessage(int ticketId, String message) {
        Message newMessage = new Message();
        try {
            newMessage.setMessage(message);
            newMessage.setTicketId(ticketId);
            _messageDao.create(newMessage);
            return true;
        } catch (SQLException sqlEx) {
            _logger.severe(String.format("Error saving ticket because:%s%s%s", sqlEx.getMessage(), System.lineSeparator(), stackTraceToString(sqlEx)));
            return false;
        }
    }
    
    public List<Ticket> getTickets() {
        List<Ticket> resultTickets = new ArrayList<>();
        try {
            resultTickets = _ticketDao.queryForEq("status", TicketStatus.Open);
            resultTickets.addAll(_ticketDao.queryForEq("status", TicketStatus.Pending));
            resultTickets.addAll(_ticketDao.queryForEq("status", TicketStatus.Other));
            return resultTickets;
        } catch (SQLException sqlEx) {
            _logger.severe(String.format("Error initializing ticket data from the database because:%s%s%s", sqlEx.getMessage(), System.lineSeparator(), stackTraceToString(sqlEx)));
            return resultTickets;
        }
    }

    public Ticket getTicket(String ticketId) {
        Ticket resultTicket = new Ticket();
        try {
            resultTicket = _ticketDao.queryForId(ticketId);
            return resultTicket;
        } catch (SQLException sqlEx) {
            _logger.severe(String.format("Error fetching ticket data from the database because:%s%s%s", sqlEx.getMessage(), System.lineSeparator(), stackTraceToString(sqlEx)));
            return resultTicket;
        }
    }

    public String[] getMessages(int ticketId) {
        try {
            List<String> messages = _messageDao.queryForEq("ticketId", ticketId).stream().map(Message::getMessage).collect(Collectors.toList());
            String[] result = new String[messages.size()];
            result = messages.toArray(result);
            return result;
        } catch (SQLException sqlEx) {
            _logger.severe(String.format("Error initializing ticket data from the database because:%s%s%s", sqlEx.getMessage(), System.lineSeparator(), stackTraceToString(sqlEx)));
            return new String[0];
        }
    }

    public void CloseDB() {
        try {
            _connSource.close();
        } catch (IOException ex) {
            _logger.severe(String.format("Error closing connection to DB:%s", ex.getMessage()));
        }
    }

    public Boolean changeStatus(String ticketId, TicketStatus status) {
        try {
            Ticket ticket = _ticketDao.queryForId(ticketId);
            ticket.setStatus(status);
            _ticketDao.update(ticket);
            return true;
        } catch (SQLException sqlEx) {
            _logger.severe(String.format("Error changng status of ticket:%s because:%s", ticketId, sqlEx.getMessage()));
            return false;
        }
    }

	private String stackTraceToString(SQLException sqlEx) {
		StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        sqlEx.printStackTrace(pw);
        String sStackTrace = sw.toString(); // stack trace as a string
        return sStackTrace;
    }
}